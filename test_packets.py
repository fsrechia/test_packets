#!/usr/bin/env python
from scapy.all import *
try:
    from scapy.contrib.mpls import *
except ImportError:
    print("Could not import contrib.mpls module. Packets with MPLS labels might not work!")
from time import strftime, localtime
import random
import sys
import argparse

#
# Unicast, Multicast and Broadcast test tool
# Author: Felipe Rechia


# List of packets sent by this tool.
valid_types = [\
'ping',\
'ping6',\
'udp',\
'udp6',\
'tcp',\
'tcp6',\
'ipfix',\
'igmpv2',\
'ldp',\
'ospf',\
'ospfdr',\
'ripv2',\
'pim',\
'vrrp',\
'igmpv3',\
'glbp',\
'stp',\
'lldp',\
'cfm',\
'arp',\
'unicastarp',\
'oam',\
'lacp',\
'cdp',\
'pagp',\
'udld',\
'vtp',\
'pvst',\
'marker',\
'gvrp',\
'dot1x',\
'ssh', \
'telnet', \
'http', \
'https', \
'dhcp',\
'snmpv2c',\
'bgp',\
'loopback',\
'l3unknown',\
'eaps',\
'dns',\
'mdns',\
'tacplus',\
'radius',\
'ntp'
]
valid_types_text = ''
for item in valid_types:
    valid_types_text += " "+item+","
valid_types_text += " all."
valid_types.append('all')
valid_types.append('experimental')


parser = argparse.ArgumentParser(description="Test tool to send dummy ICMP, UDP and TCP unicast packets, as well as multicast and broadcast packets. This tool also sends IPv6 packets based on the link-local address derived from IPv4 (FE80::A.B.C.D). The contents of the packets used to create the dummy packets were captured from samples.")
parser.add_argument('-a', '--sourceip' , nargs=1, metavar='<ip>', default=["10.255.255.250"], help='A source IP address to include in L3 IPv4 and IPv6 header. If not defined, defaults to 10.255.255.250', type=str)
parser.add_argument('-b', '--destip' , nargs=1, metavar='<ip>', default=["10.255.255.254"], help='A destination IP address to include in unicast L3 IPv4 and IPv6 header. If not defined, defaults to 10.255.255.254', type=str)
parser.add_argument('-s', '--sourcemac' , nargs=1, metavar='<mac>', default=["02:00:0a:aa:aa:aa"], help='A source MAC address to include in L2 header. If not defined, defaults to 02:00:0a:aa:aa:aa', type=str)
parser.add_argument('-d', '--destmac'    , nargs=1, metavar='<mac>', default=["02:00:0b:bb:bb:bb"], help='A destination MAC address to include in unicast L2 header. If not defined, defaults to 02:00:0b:bb:bb:bb', type=str)
parser.add_argument('-i', '--interface'    , nargs=1, metavar='<interface>', default=["lo"], help='Choose an outgoing interface (e.g., eth0) to send the packets from. Defaults to loopback interface if not defined.', type=str)
parser.add_argument('-v1', '--vlantag'    , nargs=1, metavar='<vlan>', default=[0], help='Choose a vlan tag for the outgoing packets. Defaults to untagged if none is defined.', type=int)
parser.add_argument('-v2', '--secondtag'    , nargs=1, metavar='<vlan>', default=[0], help='Choose a second vlan tag for the outgoing packets. Defaults to no second tag if none is defined.', type=int)
parser.add_argument('-c', '--count'    , nargs=1, metavar='<N>', default=[1], help='Send N packets of each type', type=int)
parser.add_argument('-p', '--prio'    , nargs=1, metavar='<prio>', default=[0], help='802.1p prio bits for the v1 vlan tag. Defaults to 0 if not specified.', type=int)
parser.add_argument('-t', '--tos'    , nargs=1, metavar='<tos>', default=[0], help='IP ToS bits. 0 if not specified', type=int)
parser.add_argument('-m', '--mpls', default=False, action='store_true', help='If this flag is set to True, encapsulate all packets in MPLS')
parser.add_argument('--mplslabels' , nargs='+', metavar='<label>', default=[100], help='Specify MPLS labels. Simply specify a list of MPLS Labels as desired. Example: --mplslabels 100 200 300.', type=int)
parser.add_argument('--mplsoutermac' , nargs=2, metavar='<outer_mac>', default=["02:01:da:7a:c0:44","02:02:da:7a:c0:44"], help='Specify outer layer MAC addresses when using MPLS', type=str)
parser.add_argument('--mplsexp' , nargs=1, metavar='<exp_bits>', default=[0], help='Specify the mpls experimental bits to be set in all mpls layers', type=int)
parser.add_argument('--mplsvlan' , nargs=1, metavar='<outer_vlan>',default=[0],help='Specify an outer vlan to be inserted before MPLS layer(s)', type=int)
parser.add_argument('-l', '--packetlist', nargs='+', metavar='<packet>', default = ['all'], help='If specified, send only packets of the chosen types for testing:'+valid_types_text+' Defaults to "all".')
parser.add_argument('--sendpfast', default=False, action='store_true', help='Save output to temporary pcap file and use sendpfast to achieve greater output bitrates')
parser.add_argument('--pps', nargs=1, metavar='<packets/s>', default=["500"], help='Specify how many packets per second sendpfast will try to achieve. Default is 500pps. Only works along with --sendpfast flag.')
parser.add_argument('--tpid', nargs=1, metavar='<tpid for double tagging>', default=["9100"], help='Specify outer tag TPID for double-tagged packets.')


args = parser.parse_args()

dest_ip=args.destip[0]
dest_mac=args.destmac[0]
source_ip=args.sourceip[0]
source_mac=args.sourcemac[0]
interface=args.interface[0]
tag1=args.vlantag[0]
tag2=args.secondtag[0]
count=args.count[0]
prio=args.prio[0]
tos=args.tos[0]
mpls=args.mpls
mplslabels=args.mplslabels
mplsoutermac=args.mplsoutermac
mplsexp=args.mplsexp[0]
mplsvlan=args.mplsvlan[0]
packetlist=args.packetlist
sendpfast_mode=args.sendpfast
pps=int(args.pps[0])
tpid=int(args.tpid[0], 16)

for packet in packetlist:
    if packet not in valid_types:
        print "Invalid packet type!"
        sys.exit(1)

if sendpfast_mode == True:
    tempfile = "/tmp/output_"+strftime('%Y-%m-%d-%Hh%Mm%Ss', localtime())+".pcap"
    print "Opening temporary pcap file: "+tempfile
    output = PcapWriter(tempfile, append=True)


if tag1 == 0:
    TAG=False
elif tag1 > 0 and tag2 == 0:
    TAG=Dot1Q(vlan=tag1,prio=prio)
elif tag1 > 0 and tag2 > 0:
    TAG=Dot1Q(type=tpid,vlan=tag1,prio=prio)/Dot1Q(vlan=tag2)



# using ipv6 link-local address according to rfc4213
# scapy will automatically convert this hybrid format to hex
source_ipv6="FE80::"+source_ip
dest_ipv6="FE80::"+dest_ip

# unique local ipv6 addresses?
#source_ipv6="fc00:a:a:abb::aff:fffa"
#dest_ipv6="fc00:a:a:abb::aff:fffe"

##########################
# MPLS
##########################

if mpls:
    for i,label in enumerate(mplslabels):
        if label == 0:
            break
        elif i==0:
            if mplsvlan != 0:
                # /"\x00\x00\x00\x00" is the 0 sequence number for the control word
                MPLS_STACK=Ether(src=mplsoutermac[0],dst=mplsoutermac[1])/Dot1Q(vlan=mplsvlan)/MPLS(label=label,cos=mplsexp)/"\x00\x00\x00\x00"
            else:
                MPLS_STACK=Ether(src=mplsoutermac[0],dst=mplsoutermac[1])/MPLS(label=label,cos=mplsexp)/"\x00\x00\x00\x00"
        elif i>0:
            MPLS_STACK=MPLS_STACK/MPLS(label=label,cos=mplsexp)/"\x00\x00\x00\x00"

##########################
# Read bin file function
##########################
# basepath
b = "./samples/bin/"
def readbin(filename):
    f = open(b+filename, 'r')
    data = f.read()
    f.close()
    return data

##########################
# Random data
##########################
# random port to be used for TCP and UDP, if necessary...
port = random.randrange(1025, 65535)

##########################
#Unicast
##########################



# generate a list of IPv4 or IPv6 unicast packets
dest_list_unicast = []
if 'ping' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["ICMP Request", 4, ICMP()])
if 'udp' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["UDP dummy scapy packet over IPV4", 4,UDP()])
if 'tcp' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["TCP dummy scapy packet over IPv4", 4,TCP()])
if 'ping6' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["ICMP Request IPV6", 6, ICMPv6EchoRequest()])
if 'udp6' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["UDP dummy scapy packet over IPv6", 6,UDP()])
if 'tcp6' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["TCP dummy scapy packet over IPv6", 6,TCP()])
if 'ipfix' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["IPFIX packet datacom 12.2-p7", 4, UDP(sport=port, dport=4739)/readbin("ipfix.bin")])
    dest_list_unicast.append(["IPFIX packet datacom 12.2-p7", 4, UDP(sport=port, dport=4739)/readbin("ipfix_26_fields.bin")])
if 'telnet' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["telnet ACK   ", 4, TCP(dport=port, sport=23,flags=0x10)])
    dest_list_unicast.append(["telnet PSH,ACK", 4, TCP(dport=port, sport=23,flags=0x18)/readbin("telnet_tcp.bin")])
if 'ssh' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["ssh: TCP SYN,ACK"  , 4,TCP(dport=port, sport=22,flags=0x12, seq=1)])
    dest_list_unicast.append(["ssh: TCP ACK"      , 4,TCP(dport=port, sport=22,flags=0x10, seq=1)])
    dest_list_unicast.append(["ssh: TCP FIN,ACK"  , 4,TCP(dport=port, sport=22,flags=0x11, seq=1)])
    dest_list_unicast.append(["ssh: SSHV2 ACK"    , 4,TCP(dport=22, sport=port, flags=0x10, seq=1)/readbin("ssh.bin")])
    dest_list_unicast.append(["ssh: SSHV2 PSH,ACK", 4,TCP(dport=22,sport=port,flags=0x18, seq=1)/readbin("ssh.bin")])
if 'http' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["http: TCP SYN", 4, TCP(dport=port, sport=80, flags="S", seq=1,options=[('MSS', 1460), ('SAckOK', ''), ('Timestamp', (27230099, 0)), ('NOP', None), ('WScale', 6)])])
    #options=readbin("http_tcp_syn_options.bin"))])
    #dest_list_unicast.append(["http: TCP ACK    ", 4, TCP(dport=port, sport=80, flags=0x10, seq=1)])
    #dest_list_unicast.append(["http: TCP FIN,ACK", 4, TCP(dport=port, sport=80, flags=0x11, seq=1)])
    #dest_list_unicast.append(["http: HTTP get   ", 4, TCP(dport=80, sport=port, flags=0x18, seq=1)/readbin("http_get.bin")])
    #dest_list_unicast.append(["http: HTTP post  ", 4, TCP(dport=80,sport=port, flags=0x18, seq=1)/readbin("http_post.bin")/readbin("http_post_line.bin")])
if 'https' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["https: TCP ACK      ", 4, TCP(dport=port, sport=443, flags=0x10, seq=1)])
    dest_list_unicast.append(["https: TCP SYN,ACK  ", 4, TCP(dport=port, sport=443, flags=0x12, seq=1)])
    dest_list_unicast.append(["https: TCP RST,ACK  ", 4, TCP(dport=port, sport=443, flags=0x14, seq=1)])
    dest_list_unicast.append(["https: HTTPS APPDATA", 4, TCP(dport=port, sport=443, flags=0x18, seq=1)/readbin("https_appdata.bin")])
    dest_list_unicast.append(["https: HTTPS CIPHER", 4, TCP(dport=port, sport=443, flags=0x18, seq=1)/readbin("https_cipher.bin")])
    dest_list_unicast.append(["https: HTTPS SERVER ", 4, TCP(sport=443, flags=0x18, seq=1)/readbin("https_server.bin")])
if 'snmpv2c' in packetlist or 'all' in packetlist:
    # <SNMP  version=<ASN1_INTEGER[1L]> community=<ASN1_STRING['public']> PDU=<SNMPget  id=<ASN1_INTEGER[0L]> error=<ASN1_INTEGER[0L]> error_index=<ASN1_INTEGER[0L]> varbindlist=[] |> |>
    dest_list_unicast.append(["SNMP get", 4,      UDP(sport=port, dport=161)/SNMP(PDU=SNMPget())])
    dest_list_unicast.append(["SNMP response", 4, UDP(dport=port, sport=161)/SNMP(PDU=SNMPresponse())])
    dest_list_unicast.append(["SNMP trap", 4,     UDP(dport=162, sport=port)/SNMP(PDU=SNMPtrapv2())])
if 'bgp' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["BGP: TCP Syn", 4,                 TCP(sport=port, dport=179,flags=0x02)])
    dest_list_unicast.append(["BGP: TCP Syn,Ack", 4,             TCP(sport=179, dport=port,flags=0x12)])
    dest_list_unicast.append(["BGP: TCP Ack", 4,                 TCP(sport=port, dport=179,flags=0x10)])
    dest_list_unicast.append(["BGP: Open", 4,                    TCP(sport=port, dport=179,flags=0x02)/readbin("bgp_open1.bin")])
    dest_list_unicast.append(["BGP: Open,Keepalive", 4,          TCP(sport=179, dport=port,flags=0x02)/readbin("bgp_open2a.bin")/readbin("bgp_open2b.bin")])
if 'dns' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["DNS query", 4,      UDP(sport=port, dport=53)/readbin("dns_query.bin")])
    dest_list_unicast.append(["DNS response", 4,      UDP(sport=53, dport=port)/readbin("dns_response.bin")])
if 'mdns' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["MDNS query", 4,      UDP(sport=5353, dport=5353)/readbin("mdns_query.bin")])
    dest_list_unicast.append(["MDNS response", 4,      UDP(sport=5353, dport=5353)/readbin("mdns_response.bin")])
if 'tacplus' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["tacplus: R Authentication", 4, TCP(dport=port, sport=49, flags=0x18, seq=1)/readbin("tacplus_r_authentication.bin")])
    dest_list_unicast.append(["tacplus: R Authorization", 4, TCP(dport=port, sport=49, flags=0x18, seq=1)/readbin("tacplus_r_authorization.bin")])
    dest_list_unicast.append(["tacplus: R Accounting", 4, TCP(dport=port, sport=49, flags=0x18, seq=1)/readbin("tacplus_r_acc.bin")])
if 'radius' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["Radius 1...", 4, UDP(dport=1812, sport=port)/readbin("radius_1.bin")])
    dest_list_unicast.append(["Radius 2...", 4, UDP(dport=port, sport=1812)/readbin("radius_2.bin")])
    dest_list_unicast.append(["Radius 3...", 4, UDP(dport=1813, sport=port)/readbin("radius_3.bin")])
    dest_list_unicast.append(["Radius 4...", 4, UDP(dport=port, sport=1813)/readbin("radius_4.bin")])
if 'ntp' in packetlist or 'all' in packetlist:
    dest_list_unicast.append(["NTP", 4, UDP(dport=port)/NTP(version=4,mode='server')])
    dest_list_unicast.append(["NTP", 4, UDP(sport=port)/NTP(version=4,mode='client')])



for LAYER4 in dest_list_unicast:
    LAYER2=Ether(src=source_mac,dst=dest_mac)

    if LAYER4[1] == 6:
        LAYER3=IPv6(src=source_ipv6,dst=dest_ipv6)
    elif LAYER4[1] == 4:
        LAYER3=IP(src=source_ip,dst=dest_ip,tos=tos)

    if not TAG:
        PACKET=LAYER2/LAYER3/LAYER4[2]
    else:
        PACKET=LAYER2/TAG/LAYER3/LAYER4[2]

    if mpls:
        PACKET=MPLS_STACK/PACKET

    if sendpfast_mode == True:
        print "Writing "+str(count)+" packet(s) to pcap file... "+LAYER4[0]
        for i in range(0,count):
            output.write(PACKET)
    else:
        print "Sending "+LAYER4[0]
        sendp(PACKET,iface=interface,count=count)

#################################################################################
# IP source fuzz packet, for L3 unknown destination testing:
# only with sendpfast for now... still not working... FIXME
#################################################################################
if 'l3unknown' in packetlist:
    LAYER2=Ether(src=source_mac,dst=dest_mac)
    print "Writing "+str(count)+" random ip source packet(s) to pcap file... "
    for i in range(0,count):
        LAYER3=fuzz(IP(dst=dest_ip,tos=tos,ttl=64,proto=1,flags=0,id=1,ihl=0,len=0x20,version=4))
        PACKET=LAYER2/LAYER3/ICMP()
        if sendpfast_mode == True:
            for i in range(0,count):
                output.write(PACKET)


#################################################################################
# IPv4 Multicast sample packets. Fields are:
# destination mac address, ip address, description, ip proto field, ttl, payload
#################################################################################

dest_list_L3_multicast = []
if 'igmpv2' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:01", "224.0.0.1", "Multicast all hosts (IGMPv2 Membership Query)", 0x02, 1,readbin("igmpv2.bin")])
if 'ldp' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:02", "224.0.0.2", "Multicast all routers (LDP Hello)", 0x11, 1,UDP(sport=646, dport=646)/readbin("ldp.bin")])
if 'ospf' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:05", "224.0.0.5", "Multicast OSPF All routers", 0x59, 1,readbin("ospf.bin")])
if 'ospfdr' in packetlist or 'all' in packetlist:
    #dest_list_L3_multicast.append(["01:00:5e:00:00:06", "224.0.0.6", "Multicast OSPF Designated Routers", 0x59, 1, ""])
    pass
if 'ripv2' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:09", "224.0.0.9", "RIP v2 Group address",0x11, 15, UDP(sport=520, dport=520)/readbin("ripv2.bin")])
if 'pim' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:0D", "224.0.0.13", "PIM v2 ALL-PIM-ROUTERS", 0x67, 1,readbin("pim.bin")])
if 'vrrp' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:12", "224.0.0.18", "VRRP", 0x70, 255,readbin("vrrp.bin")])
if 'igmpv3' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:16", "224.0.0.22", "IGMPv3", 0x02, 1,readbin("igmpv3.bin")])
if 'glbp' in packetlist or 'all' in packetlist:
    dest_list_L3_multicast.append(["01:00:5e:00:00:66", "224.0.0.102", "GLBP", 0x11, 255,UDP(sport=3222, dport=3222)/readbin("glbp.bin")])

for CONTENT in dest_list_L3_multicast:
    LAYER2=Ether(src=source_mac,dst=CONTENT[0])
    LAYER3=IP(src=source_ip,dst=CONTENT[1], proto=CONTENT[3], ttl=CONTENT[4], tos=tos)

    if not TAG:
        PACKET=LAYER2/LAYER3/CONTENT[5]
    else:
        PACKET=LAYER2/TAG/LAYER3/CONTENT[5]

    if mpls:
        PACKET=MPLS_STACK/PACKET

    if sendpfast_mode == True:
        print "Writing "+str(count)+" packet(s) to pcap file... "+CONTENT[2]+" to "+CONTENT[0]+", "+CONTENT[1]+", IP Protocol: "+str(CONTENT[3])
        for i in range(0,count):
            output.write(PACKET)
    else:
        print "Sending "+CONTENT[2]+" to "+CONTENT[0]+", "+CONTENT[1]+", IP Protocol: "+str(CONTENT[3])
        sendp(PACKET,iface=interface,count=count)

#################################################################################
# L2 Multicast sample packets.
#################################################################################


dest_list_L2_multicast = []
if 'stp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:00", "STP", 0x0026, "dot3", LLC()/STP()])
if 'lldp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:0e", "LLDP", 0x88cc, "ether", readbin("lldp.bin")])
if 'cfm' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:34", "CFM", 0x8902, "ether", readbin("cfm.bin")])
if 'dhcp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["ff:ff:ff:ff:ff:ff","DHCP Discover", 0x0800, "ether", IP(src="0.0.0.0",dst="255.255.255.255",tos=tos)/UDP(sport=67, dport=68)/readbin("dhcp_discover.bin")])
if 'arp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["ff:ff:ff:ff:ff:ff", "ARP", 0x0806, "ether", ARP()])
if 'unicastarp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append([dest_mac, "unicast ARP", 0x0806, "ether", ARP(hwsrc=source_mac,hwdst=dest_mac,psrc=source_ip,pdst=dest_ip,op=0x2)])
if 'oam' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:02", "OAM", 0x8809, "ether", readbin("oam.bin")])
if 'lacp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:02", "LACP", 0x8809, "ether", readbin("lacp.bin")])
if 'cdp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:00:0c:cc:cc:cc", "CDP", 0x0176, "dot3", LLC(dsap=0xaa, ssap=0xaa, ctrl=0x03)/"\x00\x00\x0c\x20\x00"/readbin("cdp.bin")])
if 'pagp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:00:0c:cc:cc:cc", "PAgP", 0x0046, "dot3", LLC(dsap=0xaa, ssap=0xaa, ctrl=0x03)/"\x00\x00\x0c\x01\x04"/readbin("pagp.bin")])
if 'udld' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:00:0c:cc:cc:cc", "UDLD", 0x0058, "dot3", LLC(dsap=0xaa, ssap=0xaa, ctrl=0x03)/"\x00\x00\x0c\x01\x11"/readbin("udld.bin")])
if 'vtp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:00:0c:cc:cc:cc", "VTP", 0x0055, "dot3", LLC(dsap=0xaa, ssap=0xaa, ctrl=0x03)/"\x00\x00\x0c\x20\x03"/readbin("vtp.bin")])
if 'pvst' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:00:0c:cc:cc:cd", "PVST+", 0x0032, "dot3", LLC(dsap=0xaa, ssap=0xaa, ctrl=0x03)/"\x00\x00\x0c\x01\x0b"/readbin("pvst+.bin")])
if 'marker' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:02", "marker protocol", 0x8809, "ether", readbin("marker.bin")])
if 'gvrp' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:21", "GVRP (GARP)", 0x0018, "dot3", LLC(dsap=0x42, ssap=0x42, ctrl=0x03)/readbin("gvrp.bin")])
if 'dot1x' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:03","Dot1X Request Identity", 0x888e, "ether", readbin("dot1x_1.bin")])
    dest_list_L2_multicast.append(["01:80:c2:00:00:03","Dot1X Response Identity", 0x888e, "ether", readbin("dot1x_2.bin")])
    dest_list_L2_multicast.append(["01:80:c2:00:00:03","Dot1X Request MD5-Challenge", 0x888e, "ether", readbin("dot1x_3.bin")])
    dest_list_L2_multicast.append(["01:80:c2:00:00:03","Dot1X Response MD5-Challenge", 0x888e, "ether", readbin("dot1x_4.bin")])
    dest_list_L2_multicast.append(["01:80:c2:00:00:03","Dot1X Success", 0x888e, "ether", readbin("dot1x_5.bin")])
if 'loopback' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["01:80:c2:00:00:02", "Loopback-detection", 0x8809, "ether", readbin("loopback.bin")])
if 'eaps' in packetlist or 'all' in packetlist:
    dest_list_L2_multicast.append(["00:E0:2B:00:00:04", "EAPS", 0x5C, "dot3", readbin("eaps_llc.bin")+readbin("eaps.bin")])


for CONTENT in dest_list_L2_multicast:
    if CONTENT[3] == "dot3" and not TAG:
        LAYER2=Dot3(src=source_mac,dst=CONTENT[0])
    elif CONTENT[3] == "ether" and not TAG:
        LAYER2=Ether(src=source_mac,dst=CONTENT[0],type=CONTENT[2])
    elif TAG:
        LAYER2=Ether(src=source_mac,dst=CONTENT[0])
        # packets with LLC layer use the TYPE flag as LENGTH FLAG.
        # Therefore the same logic is used to assemble the packets:
        if tag1 > 0 and tag2 == 0:
            TAG=Dot1Q(vlan=tag1, prio=prio, type=CONTENT[2])
        elif tag1 > 0 and tag2 > 0:
            TAG=Dot1Q(type=tpid, vlan=tag1, prio=prio)/Dot1Q(vlan=tag2, type=CONTENT[2])

    if not TAG:
        PACKET=LAYER2/CONTENT[4]
    else:
        PACKET=LAYER2/TAG/CONTENT[4]

    if mpls:
        PACKET=MPLS_STACK/PACKET

    if args.sendpfast == True:
        print "Writing "+str(count)+" packet(s) to pcap file... "+CONTENT[1]+". Destination MAC address: "+CONTENT[0]
        for i in range(0,count):
            output.write(PACKET)
    else:
        print "Sending "+CONTENT[1]+". Destination MAC address: "+CONTENT[0]
        sendp(PACKET,iface=interface,count=count)

#################################################################################
# Other packet types (non ethernet frames, mac pause, etc)
#################################################################################
other_packets = []
if 'experimental' in packetlist:
    other_packets.append(["EIGRPoIPX", readbin("hdlc.bin")+readbin("ipx.bin")+readbin("eigrp_hello.bin")])

for CONTENT in other_packets:
    PACKET=CONTENT[1]
    if mpls:
        PACKET=MPLS_STACK/PACKET

    if args.sendpfast == True:
        print "Writing "+str(count)+" packet(s) to pcap file... "+CONTENT[0]
        for i in range(0,count):
            output.write(PACKET)
    else:
        print "Sending "+CONTENT[0]
        sendp(PACKET,iface=interface,count=count)
#
if sendpfast_mode == True:
    print "Closing temporary pcap file: "+tempfile
    output.close()
    print "Replaying '"+tempfile+"' with sendpfast!"
    print "Attempting to replay pcap file at "+str(pps)+" packets per second..."
    sendpfast(rdpcap(tempfile),pps=pps,iface=interface,additional_options="--timer=gtod")
