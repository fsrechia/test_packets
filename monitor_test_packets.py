#!/usr/bin/python
from scapy.all import *

if len(sys.argv) > 1:
  interface=sys.argv[1]
else:
	print "No interface specified, using 'lo'"
	interface="lo"
	
filter="ether host 00:04:df:ff:ff:fc or ether host 00:04:df:ff:ff:fd or ether host 00:04:df:ff:ff:fe"

print "Filter: %s, Interface: %s" % (filter, interface)
print "Waiting for packets..."

def monitor_callback(pkt):
		return pkt.sprintf("Rx: %Ether.dst% %IP.dst%, vlan=%Ether.vlan%")

sniff(iface=interface,prn=monitor_callback, filter=filter, store=0)

